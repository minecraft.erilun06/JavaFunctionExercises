Java – Metoder
Övningar
1.	Skriv ett javaprogram som har en metod som returnerar det minsta av tre nummer.
	Skriv in första siffran: 35
	Skriv in andra siffran: 23
	Skriv in tredje siffran: 39
	// Anrop
	Det minsta talet är 23

2.	Skriv en metod i Java som beräknar och returnerar medelvärdet av tre nummer.
	Skriv in första siffran: 5
	Skriv in andra siffran: 15
	Skriv in tredje siffran: 4
	// Anrop
	Medelvärdet är 8


3.	Skriv en metod i Java som söker reda på och skriver ut den mittersta bokstaven i en textsträng.
	Att notera:
	a.	Om det är ett udda antal bokstäver i textsträngen finns det två bokstäver i mitten.
	b.	Om det är ett jämt antal bokstäver i textsträngen finns det en bokstav i mitten.

	Skriv in textsträngen: pannkakstårta
	// Anrop
	Den mittersta bokstaven är k.

4.	Skriv en metod i Java som räknar alla vokaler i en textsträng.
Skriv in textsträngen: Korv med bröd.
// Anrop
Det finns 3 vokaler.

5.	Skriv en metod i Java som räknar alla ord i en textsträng.
Skriv in textsträngen: Jag vill ha blommig falukorv till lunch.
// Anrop
Det finns 7 ord.


6.	Skriv en metod i Java som beräknar siffersumman av ett heltal. Alltså summan av talets siffror.
Skriv in siffran: 527
// Anrop
Siffersumman är 5+2+7=14


7.	Skriv en metod som skriver ut alla tecken / bokstäver mellan två givna tecken (tex C till H skriver ut C, D, E, F, G, H). Byt rad efter 10 tecken.
Skriv in första tecknet: B
Skriv in andra tecknet: W
// Anrop
B C D E F G H I J K
L M N O P Q R S T U
V W

8.	Skriv en metod som returnerar om ett givet år är skottår eller inte.
Skriv in ett årtal: 2018
// Anrop
2018 är inte ett skottår.

9.	Skriv en metod som kontrollerar om ett givet lösenord uppfyller följande regler:

a.	Lösenordet måste ha minst 10 tecken.
b.	Lösenordet måste innehålla både bokstäver och siffror.
c.	Lösenordet måste innehålla minst två siffor.

Skriv in lösenordet: M1ttSuperhemligaLösenord!
// Anrop
Lösenordet uppfyller inte villkoren:
* Det innehåller minst 10 tecken – OK
* Det innehåller både bokstäver och siffror – OK
* Det innehåller inte två eller fler siffror - FEL

10.	Skriv en metod som byter plats på två element i en array med 10 fält.
Slumpa tal mellan 1 och 50 till arrayens olika fält.
(Välj själv hur du vill att positionerna ska uppfattas, ska den första positionen vara 0 eller 1)
Arrayen innhåller följande tal: 3 34 45 19 7 30 22 3 37 16
Vilka två positioner vill du byta plats på? 1 5
// Anrop
Efter att position 1 och 5 bytt plats ser nu arrayen ut så här: 7 34 45 19 3 30 22 3 37 16







Översatt och hämtat från https://www.w3resource.com
